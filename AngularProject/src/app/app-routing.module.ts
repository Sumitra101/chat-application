import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { ForbidenComponent } from './forbiden/forbiden.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

const routes: Routes = [
  {path:'',redirectTo:'/user/login', pathMatch:'full'},
  {path:'user',component:UserComponent,
  children:[
    {path:'registration', component:RegistrationComponent},
    {path:'login', component:LoginComponent}
  ]},
  // {path:'home',component:HomeComponent ,canActivate:[AuthGuard]},
  // {path:'chatbox',component:ChatboxComponent},
  {path:'forbidden',component:ForbidenComponent},
  {path:'adminpanel',component:AdminPanelComponent ,canActivate:[AuthGuard],data:{permittedRoles:['Supervisor']}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
