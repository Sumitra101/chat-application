import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{ReactiveFormsModule , FormsModule} from "@angular/forms";
import{HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

 
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';

import { UserService } from './shared/user/user.service';
import { SignalrService } from './shared/signalr/signalr.service';

import { LoginComponent } from './user/login/login.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbidenComponent } from './forbiden/forbiden.component';
import {HomeModule} from './Home/home.module';
// import { MessagePanelComponent } from './chatbox1/message-panel/message-panel.component';
// import { SidePanelComponent } from './chatbox1/side-panel/side-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,    
    LoginComponent,
    AdminPanelComponent,
    ForbidenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    HomeModule
  ],
  providers: [UserService,SignalrService,{
    provide:HTTP_INTERCEPTORS,
    useClass:AuthInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
