import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/shared/user/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SignalrService } from 'src/app/shared/signalr/signalr.service';
import { User } from 'src/app/shared/user/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
  formModel = {
    UserName: '',
    Password: ''
  }

  constructor(private _userService: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    if (localStorage.getItem('token') != null) {
      this.router.navigateByUrl('/home');
    }
    
  }

  onSubmit(form: NgForm) {
    this._userService.login(form.value).subscribe(
      (res: any) => {
        localStorage.setItem('token', res.token);      
        // this._signalRService.startConnection();
        // this._signalRService.getConnectionId();
        this.router.navigateByUrl('/home');
      },
      err => {
        if (err.status == 400) {
          this.toastr.error('Incorrect username or password.', 'Authentication failed.')
        }
        else {
          console.log(err);
        }
      }
    );
  }




}
