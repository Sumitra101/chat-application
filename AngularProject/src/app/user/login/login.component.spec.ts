import { LoginComponent } from "./login.component"
import { of } from 'rxjs';

describe('LoginComponent', ()=>{
    let component: LoginComponent;
    let loginModel;

      let mockUserService;
      let mockRouterService;
      let mockToastrService;

    beforeEach(()=>{
        loginModel={UserName:"Acer" , Password:"1234"}
                  
        
        mockUserService=jasmine.createSpyObj(['login',])
        mockRouterService=jasmine.createSpyObj(['navigateByUrl'])
        mockToastrService=jasmine.createSpyObj(['error'])

        component=new LoginComponent(mockUserService,mockRouterService,mockToastrService);
               
    })
    describe('login', ()=>{

        it('should navigate to home page if login successful', ()=>{
            mockUserService.login.and.returnValue(of(true));
            component.formModel= loginModel;

            component.onSubmit(loginModel);

            expect(component.formModel.UserName).toEqual('Acer');
            expect(component.formModel.Password).toEqual('1234');           
        })

        it('should call login service', ()=>{
            mockUserService.login.and.returnValue(of(true));
            component.formModel= loginModel;

            component.onSubmit(loginModel);
            expect(mockUserService.login).toHaveBeenCalledWith(loginModel.value);
        })
    })

    
})