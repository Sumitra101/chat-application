export class User {
    public id:string;
    public userName: string;
    public email: string;
    public fullName: string;
    public city: string;
    public country: string;
}


    export class MessageModel{
        public id: number;
        public sender:string;
        public text:string;
        public timestamp:Date;
        public receiver:string;
        public isRead:boolean;
    }

    export class UserWithUnreadMessage{
        public name: string;
        public unreadMessage:number;
    }

