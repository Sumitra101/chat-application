import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, MessageModel } from './user.model';
// import { MessageModel } from 'src/app/chatbox/message-panel/message-panel.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _loggedInUser: User;
  
  constructor(private fb: FormBuilder, private http: HttpClient) { }

  readonly baseURI = "http://localhost:50308/api";

  userDetails:User;

  formModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FullName: [''],
    Role: [''],
    PhoneNumber:[''],
    Gender:[''],
    City:[''],
    Country:[''],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validators: this.comparePasswords })

  });

  comparePasswords(fb: FormGroup) {
    let confirmPwCtrl = fb.get('ConfirmPassword');
    if (confirmPwCtrl.errors == null || 'passwordMismatch' in confirmPwCtrl.errors) {
      if (fb.get('Password').value != confirmPwCtrl.value)
        confirmPwCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPwCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.Email,
      FullName: this.formModel.value.FullName,
      Password: this.formModel.value.Passwords.Password,
      Role: this.formModel.value.Role,
      City:this.formModel.value.City,
      Country:this.formModel.value.Country
    };
    return this.http.post(this.baseURI + '/Account/Register', body);
  }

  login(formData) {
    return this.http.post(this.baseURI + '/Account/Login', formData);
  }

  //get profile of logged in user
  getUserProfile() {
    return this.http.get<User>(this.baseURI + '/UserProfile/UserDetail');
  }

  //get list of name of all registered user 
  getListOfUser(){
    return this.http.get(this.baseURI + '/UserProfile/List');
  }

  //get list of name of all registered user 
  getListOfUserWithUnreadMessage(){
    return this.http.get(this.baseURI + '/UserProfile/ListWithUnreadMessage');
  }

//get list of userdetails except loggedin user
  getContactableUsers(){
    return this.http.get<User[]>(this.baseURI + '/UserProfile/GetContactableUsers');
  }

  //get userdetails according to userName
  getUserByUsername(username:string){
    return this.http.get<User>(this.baseURI + '/UserProfile/GetUserByUsername/'+username);
  }

  getChatHistory(user1:string,user2:string){
    return this.http.get<MessageModel[]>(this.baseURI + '/UserProfile/chatHistory/'+user1+"/"+user2);
  }

  getUnreadMessageCount(user1:string){
    return this.http.get<number>(this.baseURI + '/UserProfile/CountUnreadMessage/'+user1);
  }

  

  roleMatch(allowedRoles): boolean {
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if (userRole == element) {
        isMatch = true;
      }
    });
    return isMatch;
  }


}
