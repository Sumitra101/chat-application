import { Injectable, Output, EventEmitter } from '@angular/core';
import * as signalR from '@aspnet/signalr';

import { signalRConfig } from './signalr-config.model';
import { MessageModel } from '../user/user.model';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {



  private hubConnection: signalR.HubConnection;
  @Output() connectionId : EventEmitter<string>= new EventEmitter<string>();
  @Output() receivedMessage : EventEmitter<MessageModel> = new EventEmitter<MessageModel>();
  constructor() { }

 public startConnection=()=>{
    this.hubConnection=new signalR.HubConnectionBuilder()
            .withUrl("http://localhost:50308/chatHub")
            .build();

    this.hubConnection.start()
              .then(()=>console.log("Connection Started")).catch(err=> console.error('Connection Failed: '+err.toString())); 
    
    this.hubConnection.on("onConnected", () => {     
        this.hubConnection.on("receivePrivateMessage",(messageObj: MessageModel) => {
          console.log("receiving (inside signalr)");
          console.log(messageObj);
          this.receivedMessage.emit(messageObj);
        });
    });
  }

 public getConnectionId=()=>{
  this.hubConnection.on("onConnectedAsync", (connectionId:string) => {   
    console.log(connectionId);
    this.deleteConnectionToDb();
    this.saveConnectionIdToDb();
    this.connectionId.emit(connectionId);
  });
 }

 public saveConnectionIdToDb=()=>{
   console.log( localStorage.getItem('userName'));
  this.hubConnection.invoke("SendUserConnection", localStorage.getItem('userName'));
 }

 public deleteConnectionToDb=()=>{
  this.hubConnection.invoke("DeleteConnectionId", localStorage.getItem('userName'));
 }

 public stopConnection = () => {
  this.hubConnection.stop();
  console.log("Connection stopped");
  }

  public sendMessage(messageObj: MessageModel){
    console.log("sending"+messageObj);
    this.hubConnection.invoke("SendMessageToConnection", messageObj)
        .catch(err => console.log(err));
}
public receiveMessage(){
    this.hubConnection.on("receivePrivateMessage",(messageObj: MessageModel) => {
        console.log("receiving "+messageObj);
        this.receivedMessage.emit(messageObj);
    })
}

}
