import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home.component";
import { AuthGuard } from '../auth/auth.guard';

const homeRoutes: Routes = [
  {path:'home',component:HomeComponent ,canActivate:[AuthGuard]}
];
   
  @NgModule({
    imports: [
      RouterModule.forChild(homeRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class HomeRoutingModule { }