import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/user/user.service';
import { SignalrService } from '../shared/signalr/signalr.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../shared/user/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
public userDetails:User;
public loggedInUser:User;
public _connectionId:string="";
register=false;
toggleChatBox=false;

readonly baseURI = "http://localhost:50308/api";

  constructor(private router:Router, private _userService:UserService, private _signalRService: SignalrService , private http:HttpClient) { }
  public unreadMessage:number;
  ngOnInit() {
    this.userProfile();
  }


   userProfile(){
     this._userService.getUserProfile().subscribe(
      res=>{
        this.userDetails=res; 
        localStorage.setItem('userName', res.userName)
        // console.log(localStorage.getItem('userName', res.userName))  ; 
        this._signalRService.startConnection();
        this._signalRService.getConnectionId();
         //this._signalRService.saveConnectionIdToDb();
         this._userService.getUnreadMessageCount(localStorage.getItem('userName')).subscribe((countValue:number)=>{
            this.unreadMessage=countValue;
            console.log(this.unreadMessage)
        })
      },
      err=>{
        console.log(err);
      }
    );
    return this.userDetails;
  }

  


  onLogout(){
    this._signalRService.deleteConnectionToDb();
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    this.router.navigate(['/user/login']);
  }
  onRegister(){
    this.register = !this.register;
    this.toggleChatBox=false;
  }

  onChatBox(){
    this.toggleChatBox=!this.toggleChatBox;
    this.register=false;
    this.unreadMessage=0;
  }
}
