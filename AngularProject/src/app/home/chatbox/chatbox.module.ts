import { NgModule } from "@angular/core";

import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { ChatboxComponent } from './chatbox.component'

   
  @NgModule({
    imports: [
      CommonModule,
      FormsModule,

    ],
    declarations: [

      ChatboxComponent
    ],
    exports: [
        ChatboxComponent
    ]
  })
  export class HomeModule { }