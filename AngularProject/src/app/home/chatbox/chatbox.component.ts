import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user/user.service';
import { SignalrService } from 'src/app/shared/signalr/signalr.service';
import { User, MessageModel ,UserWithUnreadMessage} from 'src/app/shared/user/user.model';

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.css']
})
export class ChatboxComponent implements OnInit {

  public loggedInUser:string;
  public messageSeen=false;
  public messageDetail :MessageModel={
    id:0,
    sender:'',
    receiver:'',
    text:'',
    timestamp: new Date(),
    isRead:false
  };
  public message='';
  _connectionId='';
  public messageBoxToggle=false;
  public receivedMessages:MessageModel[]=[];
  public senderDetail:User={
    id:'',
    userName: '',
    email: '',
    fullName:'', 
    city: '',
    country: '',
 };
  public receiverDetail:User={
     id:'',
     userName: '',
     email: '',
     fullName:'', 
     city: '',
     country: '',
  };

  listOfUserName:string[]=[];
  listofUserWithUnreadMessage:UserWithUnreadMessage[]=[];

  constructor(private _userService:UserService ,private _signalRService: SignalrService) { }

  ngOnInit() {
    this.loggedInUser=localStorage.getItem('userName');
    this._userService.getUserProfile().subscribe(
      (res:User)=>{
        this.senderDetail=res;   
      },
      err=>{
        console.log(err);
      }
    );
    
    // this._userService.getListOfUser().subscribe((data:string[]) => {this.listOfUserName = data});
    this._userService.getListOfUserWithUnreadMessage().subscribe((data:UserWithUnreadMessage[]) => {this.listofUserWithUnreadMessage = data});
    this._signalRService.receiveMessage();
    this._signalRService.receivedMessage.subscribe((message:MessageModel)=>{
      console.log(message + "from signalr on");
      if(this.receiverDetail.userName === message.sender || this.receiverDetail.userName === message.receiver){
            this.receivedMessages.push(message);
      }
    })


  }

  
  public onSendMessage(): void {
    this.messageDetail.sender=this.senderDetail.userName;     
    this.messageDetail.receiver=this.receiverDetail.userName;
    this.messageDetail.text=this.message;
    console.log(this.messageDetail)
    this._signalRService.sendMessage(this.messageDetail);
    this.message='';
    
  }

  onClickUserButton(user:UserWithUnreadMessage){
    this._userService.getUserByUsername(user.name).subscribe((data:User)=>{
      this.receiverDetail=data;
      console.log(this.receiverDetail);     
      this._userService.getChatHistory(this.loggedInUser, this.receiverDetail.userName)
                .subscribe((messages:MessageModel[])=>{
                  this.receivedMessages=messages;
                  console.log(messages);
                });
    })
    user.unreadMessage=0;
    this.messageBoxToggle=!this.messageBoxToggle;
  }


}
