import { NgModule } from "@angular/core";

import { HomeComponent } from "./home.component";
import { HomeRoutingModule } from "./home-routing.module";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import{RegistrationModule} from "../user/registration/registration.module";
import { ChatboxComponent } from './chatbox/chatbox.component'

   
  @NgModule({
    imports: [
      CommonModule,
      FormsModule,
      HomeRoutingModule,
      RegistrationModule
    ],
    declarations: [
      HomeComponent,
      ChatboxComponent
    ],
    exports: [
      HomeComponent
    ]
  })
  export class HomeModule { }