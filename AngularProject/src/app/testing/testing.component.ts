import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user/user.service';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {
 togglevalue=false;
 chatList;
 popupUser="";
 chatBox=false;
  constructor(private _userService:UserService) { }

  ngOnInit() {
    this._userService.getListOfUser().subscribe(
      res=>{
        this.chatList=res;
      },
      err=>{
        console.log(err);
      }
    );
  }

  onclickButton(){
    this.togglevalue=!this.togglevalue;
  }

  onClickUser(chatUser){
    this.chatBox=true;
    this.popupUser=chatUser;
    console.log( this._userService.getUserByUsername(chatUser));
  }

}
