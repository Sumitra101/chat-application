﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Models
{
    public class Message
    {
        public Message()
        {
            Timestamp = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Text { get; set; }
        public DateTime Timestamp { get; set; }
        public string Receiver { get; set; }
        public bool isRead { get; set; }
    }
}
