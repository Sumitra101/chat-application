﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Column(TypeName = "nvarchar(150)")]
        public string FullName { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Country { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string City { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Gender { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public ICollection<Connection> Connections { get; set; }
    }
}
