﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Models
{
    public class Connection
    {
        [Key]
        public int id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string ConnectionID { get; set; }
    }
}
