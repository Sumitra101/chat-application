﻿using BackEnd.Data;
using BackEnd.Models;
using BackEnd.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Hubs
{

    public class ChatHub : Hub
    {
        private AppDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        public ChatHub(AppDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }



        public async Task SendMessageToConnection(Message message)            
        {
            string connectionId = null;
            var receive =await  _userManager.FindByNameAsync(message.Receiver);
            var getId = _db.Connections.Where(x => x.UserId == receive.Id);
            if (getId.Count() != 0)
            {
                connectionId = _db.Connections.FirstOrDefault(x => x.UserId == receive.Id).ConnectionID.ToString();
            }

            var messageDetail = new Message
            {
                Sender=message.Sender,
                Text = message.Text,
                Receiver = message.Receiver,
                Timestamp = message.Timestamp,
            };
            
            if (!String.IsNullOrEmpty(connectionId)){
                messageDetail.isRead = true;
                await Clients.Client(Context.ConnectionId).SendAsync("receivePrivateMessage", messageDetail);
                await Clients.Client(connectionId).SendAsync("receivePrivateMessage", messageDetail);
            }
            else
            {
                messageDetail.isRead = false;
                await Clients.Client(Context.ConnectionId).SendAsync("receivePrivateMessage", messageDetail);
            }
            _db.Messages.Add(messageDetail);
            await _db.SaveChangesAsync();
        }



        public async Task DeleteConnectionId(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var checkValue = _db.Connections.FirstOrDefault(x => x.UserId == user.Id);
            if (user != null)
            {
                if (checkValue != null)
                {
                    _db.Connections.Remove(checkValue);
                }
                await _db.SaveChangesAsync();
            }
            
        }

        public override async Task OnConnectedAsync()
        {
            //string connectionId = _db.Connections.Where(x => x
            await Clients.All.SendAsync("onConnectedAsync", $"{Context.ConnectionId}");
        }



        public async Task JoinRoom(string connectionId, string roomId)
        {
            await Groups.AddToGroupAsync(connectionId, roomId);
        }

        public async Task LeaveRoom(string connectionId, string roomId)
        {
            await Groups.RemoveFromGroupAsync(connectionId, roomId);
        }

        //public Task SendMessage(string user, string message)
        //{
        //    return Clients.All.SendAsync("SendMessage", user, message);
        //}

        public Task SendUserStatusUpdate(UserList user)
        {
            return Clients.All.SendAsync("SendUserStatusUpdate", user);
        }



        public override async Task OnDisconnectedAsync(Exception ex)
        {
            await Clients.Others.SendAsync("SendMessage", "test", $"{Context.ConnectionId} left");
        }

        public Task Send(string message)
        {
            return Clients.All.SendAsync("Send", $"{Context.ConnectionId}: {message}");
        }
        
   
        //public Task SendToConnection(string connectionId, Message message)
        //{
        //    return Clients.Client(connectionId).SendAsync("receivePrivateMessage", message);
        //}

        public Task SendToGroup(string groupName, Message message)
        {
            return Clients.Group(groupName).SendAsync("receiveSendGroupMessage", message);
        }

     


        public async Task LeaveGroup(string connectionId, string groupName)
        {
            await Clients.Group(groupName).SendAsync("Send", $"{groupName}");

            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }

        public Task Echo(string message)
        {
            return Clients.Caller.SendAsync("getMyConnection", $"{Context.ConnectionId}");
        }


        //Save or update connectionId to database
        public async Task SendUserConnection(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var role = await _userManager.GetRolesAsync(user);
            var roleInString = role.ToString();
            var checkValue = _db.Connections.FirstOrDefault(x => x.UserId == user.Id);
            if (user != null)
            {
                if (checkValue == null)
                {
                    var conn = new Connection
                    {
                        UserId = user.Id,
                        ConnectionID = Context.ConnectionId,
                    };
                    _db.Connections.Add(conn);
                }
                else
                {
                    checkValue.ConnectionID = Context.ConnectionId;
                    _db.Connections.Update(checkValue);
                }
                await _db.SaveChangesAsync();
            }
            var returnUser = new UserList
            {
                Id=user.Id,
                UserName= user.UserName,
                FullName= user.FullName,
                Email= user.Email,
                City= user.City,
                Country= user.Country,
        };
            await Clients.Others.SendAsync("recieveUserConnection", returnUser);

        }


        //save message to database and send message to client as well
        public async Task SendMessage(
            string roomId,
            string message,
            string sender,
            [FromServices] AppDbContext db)
        {
            var messageDetail = new Message
            {
                Text = message,
                Sender = sender,
                Timestamp = DateTime.Now
            };
            db.Messages.Add(messageDetail);
            await db.SaveChangesAsync();
            await Clients.Group(roomId.ToString())
                .SendAsync("ReceivedMessage", new
                {
                    Text = messageDetail.Text,
                    Name = messageDetail.Sender,
                    Timestamp = messageDetail.Timestamp.ToString("dd/MM/yyyy hh:mm:ss")
                });
        }



    }
}
