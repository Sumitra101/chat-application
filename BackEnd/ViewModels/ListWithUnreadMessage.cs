﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.ViewModels
{
    public class ListWithUnreadMessage
    {
        public string name { get; set; }
        public int unreadMessage { get; set; }
    }
}
