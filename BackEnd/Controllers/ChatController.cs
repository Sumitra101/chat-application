﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Data;
using BackEnd.Hubs;
using BackEnd.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private IHubContext<ChatHub> _chat;
        private AppDbContext _db;

        public ChatController(IHubContext<ChatHub> chat, AppDbContext db)
        {
            _chat = chat;
            _db = db;
        }

     
       

        public async Task<IActionResult> SendMessageToAll(string user, string message)
        {
            await _chat.Clients.All.SendAsync("SendMessage", user, message);
            return Ok();
        }

        public async Task<IActionResult> SendUserStatusUpdate(string user)
        {
            await _chat.Clients.All.SendAsync("SendUserStatusUpdate", user);
            return Ok();
        }
   
        public async Task<IActionResult> CreateGroup(string groupName)
        {
            await _chat.Clients.All.SendAsync("receiveCreateGroup", $"{groupName}");
            return Ok();
        }


        public async Task<IActionResult> MessageSendToConnection(string connectionId, string message)
        {
            await _chat.Clients.Client(connectionId).SendAsync("receivePrivateMessage", $"{message}");
            return Ok();
        }

        public async Task<IActionResult> MessageSendToGroup(string groupName, string message)
        {
            await _chat.Clients.Group(groupName).SendAsync("receiveSendGroupMessage", $"{message}");
            return Ok();
        }

    
        public async Task<IActionResult> JoinGroup(string connectionId, string groupName)
        {
            await _chat.Clients.Group(groupName).SendAsync("Send", $"{groupName}");
            await _chat.Groups.AddToGroupAsync(connectionId, groupName);
            return Ok();
        }

        public async Task<IActionResult> LeaveGroup(string connectionId, string groupName)
        {
            await _chat.Clients.Group(groupName).SendAsync("Send", $"{groupName}");

            await _chat.Groups.RemoveFromGroupAsync(connectionId, groupName);
            return Ok();
        }

    
    }
}