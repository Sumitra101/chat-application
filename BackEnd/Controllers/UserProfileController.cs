﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Data;
using BackEnd.Models;
using BackEnd.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private AppDbContext _db;

        public UserProfileController(UserManager<ApplicationUser> userManager, AppDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }
        [HttpGet]
        [Authorize]
        [Route("UserDetail")]
        //Get : /api/UserProfile/
        public async Task<object> GetUserProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await _userManager.FindByIdAsync(userId);
            var role = await _userManager.GetRolesAsync(user);
            var conn = _db.Connections.FirstOrDefault(x => x.UserId == userId);
            return new
            {
                user.Id,
                user.UserName,
                user.Email,
                user.FullName,                                
                user.City,
                user.Country,
                role,
            };
        }

        [HttpGet]
        [Route("chatHistory/{user1}/{user2}")]
        public async Task<IEnumerable<Message>> GetChatHistory(string user1, string user2)
        {
            var unseenMessage = _db.Messages.Where(x => x.Sender == user2 && x.isRead == false).ToList();
            if (unseenMessage.Count != 0)
            {
                foreach (var message in unseenMessage)
                {
                    message.isRead = true;
                    _db.Messages.Update(message);
                    _db.SaveChanges();
                }
            }
            
            return await _db.Messages.Where(x => x.Sender == user1 && x.Receiver == user2 ||
                                            x.Sender == user2 && x.Receiver == user1)
                                    .Select(x => new Message{
                                        Sender=x.Sender,
                                        Receiver=x.Receiver,
                                        Text=x.Text,
                                        Timestamp=x.Timestamp
                                    })
                                    .ToListAsync();
        }

        [HttpGet]
        [Authorize]
        [Route("List")]
        public List<string> GetChat()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var users = _userManager.Users
                .Where(x => x.Id != userId)
                .Select(x => x.UserName)
                .ToList();
           
            return users;

        }

        [HttpGet]
        [Authorize]
        [Route("ListWithUnreadMessage")]
        public async Task<List<ListWithUnreadMessage>> ListWithUnreadMessage()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var findUser = await _userManager.FindByIdAsync(userId);
            List<ListWithUnreadMessage> listWithUnreadMessage = new List<ListWithUnreadMessage>();
            var users = _userManager.Users
                .Where(x => x.Id != userId)
                .Select(x => x.UserName)
                .ToList();
            foreach (var user in users)
            {
                int countUnread = _db.Messages.Where(x => x.Sender == user && x.Receiver== findUser.UserName && x.isRead == false).Count();
                var list =new ListWithUnreadMessage{
                    name=user,
                    unreadMessage= countUnread
                };
                listWithUnreadMessage.Add(list);
            }
            return listWithUnreadMessage;

        }

        

        [HttpGet]
        [Authorize]
        [Route("GetUserByUsername/{username}")]
        public async Task<object> GetUserByUsername(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var role = await _userManager.GetRolesAsync(user);

            var returnValue = new UserList {
                Id=user.Id,
                UserName=user.UserName,
                Email=user.Email,
                FullName=user.FullName,
                City=user.City,
                Country=user.Country,
            };
            return returnValue;

        }

        [HttpGet]
        [Authorize]
        [Route("GetContactableUsers")]
        public  List<UserList> GetContactableUsers()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var users = _userManager.Users
                .Include(x => x.Connections)
                .Where(x => x.Id != userId)
                .Select(x => new UserList
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    Email = x.Email,
                    FullName = x.FullName,
                })
                .ToList();
            return users;           
        }


        [HttpGet]
        [Authorize]
        [Route("CountUnreadMessage/{loggedInUser}")]
        public  int GetUnreadMessageCount(string loggedInUser)
        {
            return _db.Messages.Where(x => (x.Receiver == loggedInUser) && x.isRead == false).Count();                                   
        }

  
    }
}