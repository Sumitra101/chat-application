﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BackEnd.Data;
using BackEnd.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class HomeController : ControllerBase
    {
        private AppDbContext _db;
        public HomeController(AppDbContext db) => _db = db;

        //public IActionResult Index()
        //{
        //    var chats = _db.Chats
        //        .Include(x => x.Users)
        //        .Where(x => !x.Users
        //            .Any(y => y.UserId == User.FindFirst(ClaimTypes.NameIdentifier).Value))
        //        .ToList();
        //    return Ok();
        //}

        //[HttpPost]
        //public async Task<IActionResult> CreateRoom(string name)
        //{
        //    var chat = new Chat
        //    {
        //        Name = name,
        //        Type = ChatType.Room
        //    };
        //    chat.Users.Add(new ChatUser
        //    {
        //        UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
        //        Role = "Supervisor"
        //    });
        //    _db.Chats.Add(chat);
        //    await _db.SaveChangesAsync();
        //    return Ok();
        //}
    }
}